import { Component } from '@angular/core';
import { ElementRef,ViewChild } from '@angular/core';
declare var $: any;  
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ecommerce';
  

  
  constructor(private elRef:ElementRef){}
    

  
  ngAfterViewInit() {
    $(window).load(function() {
      $('#loader').hide();
      $('html, body').css({
          overflow: '',
          height: 'auto'
      });
      $('.sticky').css({
        position: 'fixed',
        top: '0px',
        width: '100%',
      });
    });
  }
}
