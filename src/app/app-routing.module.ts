import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AboutComponent } from './pages/about/about.component';
import { BrandComponent } from './pages/brand/brand.component';
import { EcommerceComponent } from './layout/ecommerce/ecommerce.component';
import { HomeComponent } from './pages/home/home.component';
import { CartComponent } from './pages/cart/cart.component';
import { WishlistComponent } from './pages/wishlist/wishlist.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { ProductComponent } from './pages/product/product.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { Error404Component } from './pages/error404/error404.component';

export const routes: Routes = [
  {
    path: '',
    component: EcommerceComponent,
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'about',
        component: AboutComponent
      },
      {
        path: 'brand',
        component: BrandComponent
      },
      {
        path: 'cart',
        component: CartComponent
      },
      {
        path: 'wishlist',
        component: WishlistComponent
      },
      {
        path: 'registration',
        component: RegistrationComponent
      },
      {
        path: 'product',
        component: ProductComponent
      },
      {
        path: 'category-list',
        component: CategoriesComponent
      },
      { 
        path: '**', 
        pathMatch: 'full', 
        component: Error404Component 
      },
      // {
      //   path: 'apptracker',
      //   data: { title: 'Tracker' },
      //   component: AppTrackerComponent
      // },
      // {
      //   path: 'login',
      //   component: LoginComponent,
      //   data: { title: 'Login' },
      //   canActivate: [RedirectGuard]
      // },
      // {
      //   path: 'forgot-password',
      //   component: ForgotPasswordComponent,
      //   data: { title: 'Forgot Password' },
      //   canActivate: [RedirectGuard]
      // },
      // {
      //   path: 'reset-password',
      //   component: ResetPasswordComponent,
      //   data: { title: 'Reset Password' },
      //   canActivate: [RedirectGuard]
      // }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes,  {scrollPositionRestoration: 'enabled' , useHash: true })
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }