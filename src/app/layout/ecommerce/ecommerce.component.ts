import { Component, OnInit } from '@angular/core';
  import { HostListener } from '@angular/core';

@Component({
  selector: 'app-ecommerce',
  templateUrl: './ecommerce.component.html',
  styleUrls: ['./ecommerce.component.css']
})
export class EcommerceComponent implements OnInit {
  header = ''
  @HostListener("window:scroll", []) onWindowScroll() {
      // do some stuff here when the window is scrolled
      const verticalOffset = window.pageYOffset 
            || document.documentElement.scrollTop 
            || document.body.scrollTop || 0;
      if(verticalOffset>36)
       this.header= "sticky"
       else
       this.header= ""
  }
  constructor() { }

  ngOnInit(): void {
  }

}
